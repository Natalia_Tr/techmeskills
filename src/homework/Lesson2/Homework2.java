package homework;

import java.util.Random;
import java.util.Scanner;

public class Homework2 {
    public static void main(String[] args) {
    int [] myArray = new int[] {3,-7,8};
    System.out.println("Решение 1:");
    Solution1(myArray);
    System.out.println("Решение 2:");
    Solution2();
    System.out.println("Решение 3:");
    Solution3();
    System.out.println("Решение 4:");
    Solution4();
    }
    public static void Solution1 (int [] array1){   //Кол-во положительных чисел в наборе
        int numberPol = 0;
        for (int i = 0; i < array1.length; i++) {
            if (array1[i] > 0) numberPol++;
        }
        System.out.println("Кол-во положительных чисел: " + numberPol);
    }

    public static void Solution2(){     //Кол-во положительных и отрицательных чисел из введённых
        int pol = 0;
        int otr = 0;
        int [] array2 = new int[3];
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите 3 целых числа:");
        for (int i = 0; i < 3; i++){
             array2 [i] = scan.nextInt();
             if (array2[i] > 0) pol++;
             else if (array2[i] < 0) otr++;
        }
        System.out.println("Положительных чисел " + pol);
        System.out.println("Отрицательных чисел " + otr);
    }

    public static void Solution3(){     //Поиск минимального эл-та в массиве
        int [] myArray = new int[10];
        myArray [0] = RandomArray();
        int min = myArray [0];
        System.out.println("Поиск минимального числа в этом массиве:");
        System.out.print(myArray[0] + " ");
        for (int i = 1; i < 10; i++){
            myArray [i] = RandomArray();
            System.out.print(myArray[i] + " ");
            if (myArray[i]<min) min = myArray[i];
        }
        System.out.println();
        System.out.println("Минимальное число в массиве: " + min);
    }
    public static int RandomArray(){
        Random rand = new Random();
        return rand.nextInt(100);
    }
    public static void Solution4 (){      //Вывод на экран кол-ва программистов
        System.out.println("Введите кол-во программистов (до 10):");
        Scanner scan = new Scanner(System.in);
        int numProgr = scan.nextInt();
        switch (numProgr){
            case 1:
                System.out.println(numProgr + " программист");
                break;
            case 2:
            case 3:
            case 4:
                System.out.println(numProgr + " программиста");
                break;
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                System.out.println(numProgr + " программистов");
                break;
            default:
                System.out.println("Недопустимое значение!");
             }
    }
}
