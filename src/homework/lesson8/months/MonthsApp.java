package homework.lesson8.months;

import java.util.Scanner;

public class MonthsApp {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите номер месяца: ");
        int numMonth = scan.nextInt();

        Months.getValueOfMonth(numMonth);
    }
}
