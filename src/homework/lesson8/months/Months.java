package homework.lesson8.months;

public enum Months {
    JANUARY(1,"Январь"),
    FEBRUARY(2,"Февраль"),
    MARCH(3,"Март"),
    APRIL(4,"Апрель"),
    MAY(5,"Май"),
    JUNE(6,"Июнь"),
    JULY(7,"Июль"),
    AUGUST(8,"Август"),
    SEPTEMBER(9,"Сентябрь"),
    OCTOBER(10,"Октябрь"),
    NOVEMBER(11,"Ноябрь"),
    DECEMBER(12,"Декабрь");

    String monthValue;
    int numberOfMonth;

    Months(int numberOfMonth, String monthValue){
        this.numberOfMonth = numberOfMonth;
        this.monthValue=monthValue;
    }

    public static void getValueOfMonth(int scan){
        Months[] months = Months.values();
        if (scan>0 && scan<=12) {
            for (int i = 0; i < months.length; i++) {
                if (months[i].numberOfMonth == scan) {
                    System.out.println(months[i].monthValue);
                }
            }
        }
        else System.out.println("Неверный номер месяца!");

    }
}
