package homework.lesson8.shape;

import java.util.Scanner;

public class ShapeCalculation {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        try {
            Rectangle rectangle = new Rectangle();
            System.out.println("Введите ширину прямоугольника:");
            rectangle.setWidth(scan.nextInt());
            System.out.println("Введите высоту прямоугольника:");
            rectangle.setHeight(scan.nextInt());

            Circle circle = new Circle();
            System.out.println("Введите радиус круга:");
            circle.setRadius(scan.nextInt());


            callGetArea(rectangle);
            callGetArea(circle);

            System.out.println();

            callGetPerimeter(rectangle);
            callGetPerimeter(circle);
        } catch (Exception e) {
            System.out.println("Введено неверное значение!");
        }
    }

    public static void callGetArea(Shape figure) {
        figure.getArea();
    }

    public static void callGetPerimeter(Shape figure) {
        figure.getPerimeter();
    }
}
