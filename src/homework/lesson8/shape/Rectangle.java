package homework.lesson8.shape;

public class Rectangle extends Shape {
    private int width;
    private int height;

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public void getArea() {
        area = width * height;
        System.out.printf("Площать прямоугольника со сторонами %d и %d равна %d\n", width, height,(int) area);
    }

    @Override
    public void getPerimeter() {
        perimeter = 2 * (width + height);
        System.out.printf("Периметр прямоугольника со сторонами %d и %d равен %d\n", width, height, (int) perimeter);
    }
}
