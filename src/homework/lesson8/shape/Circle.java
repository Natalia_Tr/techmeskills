package homework.lesson8.shape;

public class Circle extends Shape {
    private static final double PI = 3.1415926;
    private int radius;

    public void setRadius(int radius) {
        this.radius = radius;
    }

    @Override
    public void getArea() {
        area = PI * radius * radius;
        System.out.printf("Площать круга радиусом %d равна %f\n",radius, area);
    }

    @Override
    public void getPerimeter() {
        perimeter = 2 * PI * radius;
        System.out.printf("Длина окружности радиусом %d равна %f\n", radius, perimeter);
    }
}
