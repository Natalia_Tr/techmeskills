package homework.lesson8.shape;

public abstract class Shape {
    protected double area;
    protected double perimeter;
    public abstract void getArea();
    public abstract void getPerimeter();
}
