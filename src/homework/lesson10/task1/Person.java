package homework.lesson10.task1;

public class Person {
    //Объявляем свойства объекта класса Person
    private int id;
    private String name;

    //Конструктор с пустым телом
    public Person() {
    }

    //Конструктор, в котором у объекта сразу заполняется поле id (только id, мне так захотелось :)
    Person(int id) {
        this.id = id;
    }

    //Метод для присвоения полю объекта id значения, переданного из класса PersonService
    public void setId(int id) {
        this.id = id;
    }

    //Метод для присвоения полю объекта name значения, переданного из класса PersonService
    public void setName(String name) {
        this.name = name;
    }

    //Метод, возвращающий значение поля id в класс PersonService
    public int getId() {
        return id;
    }

    //Метод, возвращающий значение поля name в класс PersonService
    public String getName() {
        return name;
    }

    //Метод для вывода на экран id и name объекта. Их мы получаем из геттеров, переданных в качестве параметров
    public void printInfo(int id, String name) {
        System.out.println("Person " + id + " name " + name);   //здесь id и name - это локальные переменные, параметры,
                                                                // переданные при вызове этого метода в классе PersonService
    }

    }
