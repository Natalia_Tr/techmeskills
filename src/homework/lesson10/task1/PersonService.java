package homework.lesson10.task1;

public class PersonService {
    public static void main(String[] args) {

        //создание объекта person1. Используем пустой конструктор
        Person person1 = new Person();

        //Инициализация полей объекта person1 через сеттеры
        person1.setId(1);
        person1.setName("Ivan");

        //создание объекта person2. Используем конструктор, в который сразу передаём значение поля id
        Person person2 = new Person(2);

        //Инициализация поля name объекта person2 с помощью метода setName
        person2.setName("Petr");

        //Получение имён (полей name) наших объектов из класса Person. Для этого вызываем метод getName у каждого объекта
        System.out.println("Persons' names: " + person1.getName() + " and " + person2.getName());

        //Выводим на экран информацию об объектах (значения их полей).
        //В качестве параметров выступают возвращаемые из геттеров значения
        person1.printInfo(person1.getId(), person1.getName());
        person2.printInfo(person2.getId(), person2.getName());

    }

}
