package homework.lesson10.task3;

import java.util.Scanner;

public class Task3StringBuilder {
    public static void main(String[] args) {

        System.out.println("Ввдите количество строк: ");
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();

        String[] arrStrings = new String[n];
        int allStringsLength = 0;

        System.out.println("Введите строки");
        for (int i = 0; i < arrStrings.length; i++) {
            arrStrings[i] = scan.next();                                //заполняем массив строк
            allStringsLength = allStringsLength + arrStrings[i].length(); //считаем сумму длин всех строк
        }

        int averageLength = allStringsLength / n;    //средняя длина строки
        System.out.println("Средняя длина строки " + averageLength);

        System.out.println("\nСтроки длинее средней: ");

        StringBuilder strb = new StringBuilder();
        for (int i = 0; i < n; i++) {
            if (arrStrings[i].length() > averageLength) {
                strb.append("\"").append(arrStrings[i]).append("\"").append("  длина строки  ").append(arrStrings[i].length()).append("\n");
            }
        }
        System.out.println(strb);
    }
}
