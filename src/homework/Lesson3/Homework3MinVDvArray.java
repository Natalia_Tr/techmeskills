package homework;

public class Homework3MinVDvArray {
    public static void main(String[] args) {
        int[][] myArray = new int[][]{{2, 5, -3, -7}, {6, -1, 2, 0}, {18, 4, 0, 36}};
        int min = myArray[0][0];
        for (int i = 0; i < myArray.length; i++) {
            for (int j = 0; j < myArray[i].length; j++) {
                if (myArray[i][j] < min) min = myArray[i][j];
            }
        }

        for (int i = 0; i < myArray.length; i++) {
            for (int j = 0; j < myArray[i].length; j++) {
                System.out.print(" " + myArray[i][j]);
            }
            System.out.println();
        }
        System.out.println("Минимальный эл-т массива " + min);
    }
}
