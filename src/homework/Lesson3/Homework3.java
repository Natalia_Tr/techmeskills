package homework;

import java.util.Random;
import java.util.Scanner;

public class Homework3 {
    public static void sortMax(int[] arrayMax) {
        int tmp = arrayMax[0];
        for (int i = 0; i < arrayMax.length; i++) {
            for (int j = 0; j < arrayMax.length - 1; j++) {
                if (arrayMax[j] > arrayMax[j + 1]) {
                    tmp = arrayMax[j];
                    arrayMax[j] = arrayMax[j + 1];
                    arrayMax[j + 1] = tmp;
                }
            }
        }
    }

    public static void sortMin(int[] arrayMin) {
        int tmp = arrayMin[0];
        for (int i = 0; i < arrayMin.length; i++) {
            for (int j = 0; j < arrayMin.length - 1; j++) {
                if (arrayMin[j] < arrayMin[j + 1]) {
                    tmp = arrayMin[j];
                    arrayMin[j] = arrayMin[j + 1];
                    arrayMin[j + 1] = tmp;
                }
            }
        }
    }

    public static void main(String[] args) {
        System.out.println("Введите кол-во эл-тов массива");
        Scanner scan = new Scanner(System.in);
        int[] myArray = new int[scan.nextInt()];
        Random rand = new Random();
        for (int i = 0; i < myArray.length; myArray[i++] = rand.nextInt(100)) ;               //заполнение массива
        System.out.println("Первоначальный массив:");   //вывод первоначального массива на экран
        for (int a : myArray)
            System.out.print("  " + a);
        System.out.println();
        int choice = 0;
        System.out.println("Выберете метод сортировки:");
        System.out.println("0 - по убыванию");
        System.out.println("1 - по возрастанию");
        choice = scan.nextInt();
        while (choice == 0 || choice == 1) {
            switch (choice) {
                case 0:
                    System.out.println("Сортировка по убыванию:");
                    sortMin(myArray);
                    for (int a : myArray) {
                        System.out.print("  " + a);
                    }
                    break;
                case 1:
                    System.out.println("Сортировка по возрастанию:");
                    sortMax(myArray);
                    for (int a : myArray) {
                        System.out.print("  " + a);
                    }
                    break;
            }
            System.out.println();
            System.out.println("0 - по убыванию");
            System.out.println("1 - по возрастанию");
            System.out.println("другое число - выйти из программы");
            choice = scan.nextInt();
        }
        System.out.println("End");
    }
}
