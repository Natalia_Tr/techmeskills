package homework;

import java.util.Random;
import java.util.Scanner;

public class Homework3StupidSort {

    private static void stupidSort(int[] arraySort) {
        for (int i = 0; i < arraySort.length - 1; i++) {
            if (arraySort[i] > arraySort[i + 1]) {
                int tmp = arraySort[i];
                arraySort[i] = arraySort[i + 1];
                arraySort[i + 1] = tmp;
                i = -1;
            }
        }
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите кол-во эл-тов массива");
        int[] myArray = new int[] {1,7,5,-3,0,23,6};
        Random rand = new Random();
      //  for (int i = 0; i < myArray.length; myArray[i++] = rand.nextInt(100)) ;

        System.out.println("Первоначальный массив");
        for (int a : myArray) System.out.print(" " + a);
        stupidSort(myArray);
        System.out.println();
        System.out.println("Отсортированный по возрастанию массив");
        for (int a : myArray) System.out.print(" " + a);
    }
}
