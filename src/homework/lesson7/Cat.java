package homework.lesson7;

public class Cat extends Animal{
    public String eyesColor;

    public void setEyesColor(String eyesColor){
        this.eyesColor = eyesColor;
    }


    public void printInfo() {
        System.out.printf("Cat ID: %d ; name %s with %s eyes, date %s \n", animalID, name, eyesColor, data);
    }
}
