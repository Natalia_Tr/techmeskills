package homework.lesson7;

public class Tiger extends Cat{
    public int countEatenExployees;

    public void setCountEatenExployees(int countEatenExployees) {
        this.countEatenExployees = countEatenExployees;
    }

    @Override
    public void printInfo() {
        System.out.printf("Tiger ID: %d ; name %s with %s eyes eat %d exployees on date %s \n", animalID, name, eyesColor, countEatenExployees, data);
    }
}
