package homework.lesson7;

public class Dog extends Animal{
    public double weight;

    public void setWeight(double weight){
        this.weight = weight;
    }

    public void printInfo() {
        System.out.printf("Dog ID: %d ; name %s with weight %f, date %s \n", animalID, name, weight, data);;
    }
}
