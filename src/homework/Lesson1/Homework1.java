package homework.lesson1;

public class Homework1 {
    public static void main(String[] args) {

        // Задание 1. Даны 2 числа. Вывести большее из них.
        System.out.println("Задание 1:");
        int a1 = 5;
        double b1 = 3.8;
        if (a1>b1){
            System.out.println(a1);
        }
        else {
            System.out.println(b1);
        }

        // Задание 2. Даны 2 числа. Вывести меньшее из них.
        System.out.println("Задание 2:");
        short a2 = 112;
        float b2 = 3.1F;
        if (a2<b2){
            System.out.println(a2);
        }
        else {
            System.out.println(b2);
        }

        // Задание 3. Даны 2 числа, вывести их сумму.
        System.out.println("Задание 3:");
        int a3 = 67;
        long b3 = 120000;
        long c3 = a3 + b3;
        System.out.println(c3);

        // Задание 4. Даны 2 числа, если они равны, то вывести true, если не равны, то false.
        System.out.println("Задание 4:");
        short a4 = 34;
        int b4 = 34;
        if (a4==b4){
            System.out.println("true");
        }
        else {
            System.out.println("false");
        }

        // Задание 5. Даны 2 числа, если их сумма больше 4, то вывести true, иначе false
        System.out.println("Задание 5:");
        int a5 = 1;
        double b5 = 2.7;
        boolean c5 = (a5 + b5) > 4;
        System.out.println(c5);

        // Задание 6. Даны 2 числа с плавающей точкой, преобразовать их сумму к целочисленному значению.
        System.out.println("Задание 6:");
        float a6 = 2.4F;
        double b6 = 6.17;
        int c6 = (int) (a6 + b6);
        System.out.println(c6);

        // Задание 7. Даны 4 числа, если хотя бы одна переменная больше нуля, то вывести true, иначе false.
        System.out.println("Задание 7:");
        int a7 = -4;
        int b7 = -6;
        int c7 = 34;
        int d7 = -456;
        boolean e7 = (a7>0)||(b7>0)||(c7>0)||(d7>0);
        System.out.println(e7);

    }
}
