package homework.lesson6;

public class Student {

    Student(String name, int group, int grade){
        this.name = name;
        this.group = group;
        this.grade = grade;
    }

    public String name;
    public int group;
    public int grade;

    /*
    Тот самый метод, который будет выводить всю информацию о студенте.
     */

    public void studentInfo()
    {
        System.out.println("Имя студента " + name + " группа " + group + " оценка " + grade);
    }

    @Override
    public String toString()
    {
        return "Name " + name + " group " + group + " Grade " + grade;
    }

}
