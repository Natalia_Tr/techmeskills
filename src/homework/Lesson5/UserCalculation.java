package homework.Lesson5;

import java.util.Random;
import java.util.Scanner;

public class UserCalculation {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Random rand = new Random();
        System.out.println("Введите количество пользователей");
        User[] users = new User[scan.nextInt()];
        String[] names = new String[]{"Иван", "Мария", "Петр", "Михаил", "Юлия"};
        String[] logins = new String[]{"log1", "log2", "log3", "log4", "log5", "log6", "log7"};
        for (int i = 0; i < users.length; i++) {
            User user = new User();
            user.setName(names[rand.nextInt(5)]);
            user.setLogin(logins[rand.nextInt(7)]);
            users[i] = user;
        }

        for (int i = 0; i < users.length; i++) {
            System.out.println(users[i].toString());
        }
    }
}
