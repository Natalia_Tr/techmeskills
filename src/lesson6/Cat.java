package lesson6;

public class Cat {
    String name;
    int age;
    int gramm;

    Cat(String name, int age, int gramm) {
        this.name = name;
        this.age = age;
        this.gramm = gramm;
    }

    @Override
    public String toString() {
        return name + age + gramm;
    }
}
