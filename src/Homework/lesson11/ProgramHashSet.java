package homework.lesson11;

import java.util.HashSet;
import java.util.Set;

public class ProgramHashSet {
    public static void main(String[] args) {

        Set<String> states = new HashSet<String>();
        //добавим в список ряд элементов
        states.add("Germany");
        states.add("France");
        states.add("Italy");

        //пытаемся добавить элемент, который уже есть в коллекции
        boolean isAdded = states.add("Germany");
        System.out.println(isAdded);    //false

        //выводим кол-во элементов коллекции
        System.out.printf("Set contains %d elements \n", states.size());

        //выводим на экран коллекцию
        for (String state : states) {
            System.out.print(state+ " ");
        }

        //удаление элемента
        states.remove("Germany");

        //снова выводим на экран коллекцию
        System.out.println();
        for (String state : states) {
            System.out.print(state+" ");
        }

        //хеш-таблица объектов Person
        Set<Person> people = new HashSet<Person>();
        people.add(new Person("Mike"));
        people.add(new Person("Tom"));
        people.add(new Person("Nick"));
        System.out.println();
        for (Person p : people) {
            System.out.print(p.getName()+" ");
        }
    }
}
